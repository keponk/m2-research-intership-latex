# M2-Research Intership Latex

The available latex file feels outdated and uses latex's `memoir`-class default style. Until no style is officially put forward the key element is the cover page with the Paris-Sud logo. More than that, the style `memoir` feels more oriented to a long-winded document with chapters (as the memoir class defaults to). The 20-25 pages [suggested by the department](https://sites.google.com/view/master-hcid-hci/internships/general-info#h.p_5bCFVdEjFGO5) feel closer to an  `article` class.

The current template is available at https://www.lri.fr/~cfleury/M2R-template/. It would be better that the CS/HCI department hosts and maintains an approved latex template in an official account. Possibly in the UPSud official gitlab instance (https://gitlab.u-psud.fr/).

## Usage
 * Use `thesistemplate.tex` as a starting point.
 * Bibliography file is `references.bib` in bibtex format .
 
## Notes
 * Document uses ACM citation format since HCI department at UPSud seems more familir with it. Check with your thesis advisor.
 * Recommend using Zotero or Medeley apps for managing your bibliography.
 
## TO-DO
 * Create upsud.cls class file.
 * Move all page config and styling into the custom class.
 
## Cover Page Screenshot
![cover page screenshot](images/coverpagesample.png)